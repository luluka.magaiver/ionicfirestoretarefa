scheduled = [];

  constructor(
    private plt: Platform,
    private localNotifications: LocalNotifications,
    private alertCtrl: AlertController
  ) {
    this.plt.ready().then(() => {
      this.localNotifications.on('click').subscribe(res => {
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      });

      this.localNotifications.on('trigger').subscribe(res => {
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg);
      });
    });
  }

  scheduleNotification() {
    this.localNotifications.schedule({
      id: 1,
      title: 'Attention',
      text: 'Simons Notification',
      data: { mydata: 'My hidden message this is' },
      trigger: { in: 5, unit: ELocalNotificationTriggerUnit.SECOND },
      foreground: true // Show the notification while app is open
    });
  }

  showAlert(header, sub, msg) {
    this.alertCtrl
      .create({
        header: header,
        subHeader: sub,
        message: msg,
        buttons: ['Ok']
      })
      .then(alert => alert.present());
  }

  recurringNotification() {
    this.localNotifications.schedule({
      id: 22,
      title: 'Recurring',
      text: 'Simons Recurring Notification',
      trigger: { every: ELocalNotificationTriggerUnit.MINUTE }
    });
  }

  repeatingDaily() {
    this.localNotifications.schedule({
      id: 42,
      title: 'Good Morning',
      text: 'Code something epic today!',
      trigger: { every: { hour: 9, minute: 30 } }
    });
  }

  getAll() {
    this.localNotifications.getAll().then((res: ILocalNotification[]) => {
      this.scheduled = res;
    });
  }

  \\html


  <ion-button
    expand="block"
    (click)="scheduleNotification()"
  >
    Schedule
  </ion-button>
  <ion-button
    expand="block"
    (click)="recurringNotification()"
  >
    Every Minute
  </ion-button>
  <ion-button
    expand="block"
    (click)="repeatingDaily()"
  >
    Daily
  </ion-button>
  <ion-button
    expand="block"
    (click)="getAll()"
  >
    Get All
  </ion-button>

  <ion-list>
    <ion-item *ngFor="let n of scheduled">
      <ion-label text-wrap>
        {{ n.id }} - {{ n.title }}
        <p>Trigger: {{ n.trigger | json }}</p>
      </ion-label>
    </ion-item>
  </ion-list>