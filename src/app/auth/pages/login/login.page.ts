import { ActivatedRoute } from '@angular/router';
import { auth } from 'firebase';
import { AuthService } from './../../../core/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthProvider } from 'src/app/core/service/auth.types';
import { OverlayService } from 'src/app/core/service/overlay.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  authForm: FormGroup;
  authProviders = AuthProvider;
  configs = {
    isSignIn: true,
    action: 'Login',
    actionChange: 'Criar uma Conta'
  };

  private nameControl = new FormControl('', [Validators.required, Validators.minLength(3)]);
  private cityControl = new FormControl('', [Validators.required, Validators.minLength(3)]);

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private overlayService: OverlayService,
    private naveCtrl: NavController,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.authForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  changeAuhtAction() {
    this.configs.isSignIn = !this.configs.isSignIn;
    const { isSignIn } = this.configs;
    this.configs.action = isSignIn ? 'Login' : 'Inscrever-se';
    this.configs.actionChange = isSignIn ? 'Criar uma Conta' : 'Já tenho uma Conta';
    !isSignIn
      ? this.authForm.addControl('name', this.nameControl)
      : this.authForm.removeControl('name');
    // !isSignIn
    //   ? this.authForm.addControl('city', this.cityControl)
    //   : this.authForm.removeControl('city');
  }

  async onSubmit(provider: AuthProvider): Promise<void> {
    const loading = await this.overlayService.loading();
    try {
      const ativacao = await this.authService.authenticate({
        isSignIn: this.configs.isSignIn,
        user: this.authForm.value,
        provider
      });
      this.naveCtrl.navigateForward(
        this.route.snapshot.queryParamMap.get('redirect') || '/tarefas'
      );
    } catch (e) {
      console.error('Error: ', e);
      await this.overlayService.toast({
        message: e.message
      });
    } finally {
      loading.dismiss();
    }
  }

  get city(): FormControl {
    return <FormControl>this.authForm.get('city');
  }

  get name(): FormControl {
    return <FormControl>this.authForm.get('name');
  }

  get email(): FormControl {
    return <FormControl>this.authForm.get('email');
  }

  get password(): FormControl {
    return <FormControl>this.authForm.get('password');
  }
}
