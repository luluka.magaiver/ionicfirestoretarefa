import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './core/service/auth.service';

import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  pages: {
    url?: string;
    direction?: string;
    icone?: string;
    texto?: string;
    imagem?: string;
    children: Array<any>;
  }[];
  user: firebase.User;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private fcm: FCM,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.pages = [
      {
        icone: 'home',
        texto: 'Inicio',
        children: [
          {
            url: '/tarefas',
            direction: 'root',
            icone: 'checkmark',
            texto: 'Tarefas',
            children: []
          },
          {
            url: '/tarefas/criar',
            direction: 'root',
            icone: 'add',
            texto: 'Criar',
            children: []
          },
          {
            url: '/politica-privacidade',
            direction: 'root',
            icone: 'lock',
            texto: 'Politica de Privacidade',
            children: []
          }
        ]
      },
      {
        icone: 'log-out',
        texto: 'Sair',
        children: []
      }
    ];
    this.authService.authState$.subscribe(user => (this.user = user));
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.fcm.getToken().then(token => {
      console.log(token);
    });

    // this.fcm.onTokenRefresh().subscribe(token => {
    //   console.log(token);
    // });

    this.fcm.subscribeToTopic('teste1').then(resp => {
      console.log('Resposta do topico teste1: ' + resp);
    });

    this.fcm.subscribeToTopic('teste2').then(resp => {
      console.log('Resposta do topico teste2: ' + resp);
    });

    // this.fcm.onNotification().subscribe(data => {
    //   console.log(data);
    //   if (data.wasTapped) {
    //     console.log('Received in background');
    //     this.router.navigate([data.landing_page, data.price]);
    //   } else {
    //     console.log('Received in foreground');
    //     this.router.navigate([data.landing_page, data.price]);
    //   }
    // });
  }
}
