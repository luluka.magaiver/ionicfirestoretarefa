import { NgModule } from '@angular/core';

import { TarefaRoutingModule } from './tarefa-routing.module';

@NgModule({
  declarations: [],
  imports: [TarefaRoutingModule]
})
export class TarefaModule {}
