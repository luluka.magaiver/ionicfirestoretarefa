import { AuthService } from './../../core/service/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Tarefa } from './../models/tarefa.model';
import { Injectable } from '@angular/core';
import { Firestore } from 'src/app/core/classes/firestore.class';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class TarefaService extends Firestore<Tarefa> {
  constructor(db: AngularFirestore, private authService: AuthService) {
    super(db);
    this.init();
  }

  private init(): void {
    this.authService.authState$.subscribe(user => {
      if (user) {
        this.setConlections(`/users/${user.uid}/tarefa`, ref =>
          ref.orderBy('done', 'asc').orderBy('title', 'asc')
        );
        return;
      }

      this.setConlections(null);
    });
  }
}
