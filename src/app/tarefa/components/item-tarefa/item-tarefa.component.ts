import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tarefa } from '../../models/tarefa.model';

@Component({
  selector: 'app-item-tarefa',
  templateUrl: './item-tarefa.component.html',
  styleUrls: ['./item-tarefa.component.scss']
})
export class ItemTarefaComponent {
  @Input() tarefa: Tarefa;
  @Output() done = new EventEmitter<Tarefa>();
  @Output() update = new EventEmitter<Tarefa>();
  @Output() delete = new EventEmitter<Tarefa>();
}
