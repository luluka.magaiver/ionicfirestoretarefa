import { ItemTarefaComponent } from './item-tarefa/item-tarefa.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ItemTarefaComponent],
  exports: [ItemTarefaComponent],
  imports: [SharedModule]
})
export class ComponentsModule {}
