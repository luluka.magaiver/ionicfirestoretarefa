import { AuthGuard } from './../core/guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'criar',
        loadChildren: './pages/salvar-tarefa/salvar-tarefa.module#SalvarTarefaPageModule'
      },
      {
        path: 'editar/:id',
        loadChildren: './pages/salvar-tarefa/salvar-tarefa.module#SalvarTarefaPageModule'
      },
      {
        path: '',
        loadChildren: './pages/lista-tarefa/lista-tarefa.module#ListaTarefaPageModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TarefaRoutingModule {}
