import { ActivatedRoute } from '@angular/router';
import { TarefaService } from './../../services/tarefa.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/service/overlay.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-salvar-tarefa',
  templateUrl: './salvar-tarefa.page.html',
  styleUrls: ['./salvar-tarefa.page.scss']
})
export class SalvarTarefaPage implements OnInit {
  tarefasForm: FormGroup;
  pagetitle = '...';
  tarefaID: string;

  constructor(
    private fb: FormBuilder,
    private navCtrl: NavController,
    private overlayService: OverlayService,
    private tarefaService: TarefaService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.criarform();
    this.init();
  }

  init(): void {
    const tarefaid = this.route.snapshot.paramMap.get('id');
    if (!tarefaid) {
      this.pagetitle = 'Criar Tarefa';
      return;
    }

    this.tarefaID = tarefaid;
    this.pagetitle = 'Editar Tarefa';
    this.tarefaService
      .get(tarefaid)
      .pipe(take(1))
      .subscribe(({ title, done }) => {
        this.tarefasForm.get('title').setValue(title);
        this.tarefasForm.get('done').setValue(done);
      });
  }

  private criarform(): void {
    this.tarefasForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(3)]],
      done: [false]
    });
  }

  async onSubmit(): Promise<void> {
    const loading = await this.overlayService.loading({
      message: 'Salvando...'
    });
    try {
      const tarefa = !this.tarefaID
        ? await this.tarefaService.create(this.tarefasForm.value)
        : await this.tarefaService.update({
            id: this.tarefaID,
            ...this.tarefasForm.value
          });
      console.log('Tarefa salva! ', tarefa);

      this.navCtrl.navigateBack('/tarefas');
    } catch (error) {
      await this.overlayService.toast({
        message: error.message
      });
      console.error('Erro ao criar tarefa', error);
    } finally {
      loading.dismiss();
    }
  }
}
