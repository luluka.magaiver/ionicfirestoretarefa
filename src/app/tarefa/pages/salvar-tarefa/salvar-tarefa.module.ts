import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalvarTarefaPage } from './salvar-tarefa.page';

const routes: Routes = [
  {
    path: '',
    component: SalvarTarefaPage
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [SalvarTarefaPage]
})
export class SalvarTarefaPageModule {}
