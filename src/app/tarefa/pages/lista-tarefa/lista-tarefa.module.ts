import { ComponentsModule } from './../../components/components.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaTarefaPage } from './lista-tarefa.page';

const routes: Routes = [
  {
    path: '',
    component: ListaTarefaPage
  }
];

@NgModule({
  imports: [SharedModule, ComponentsModule, RouterModule.forChild(routes)],
  declarations: [ListaTarefaPage]
})
export class ListaTarefaPageModule {}
