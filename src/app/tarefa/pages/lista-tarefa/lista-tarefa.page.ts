import { NavController } from '@ionic/angular';
import { TarefaService } from './../../services/tarefa.service';
import { Tarefa } from './../../models/tarefa.model';
import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { OverlayService } from 'src/app/core/service/overlay.service';
import { take } from 'rxjs/operators';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-lista-tarefa',
  templateUrl: './lista-tarefa.page.html',
  styleUrls: ['./lista-tarefa.page.scss']
})
export class ListaTarefaPage implements OnInit {
  tarefas$: Observable<Tarefa[]>;
  tarefas: Tarefa[];
  qtTarefasFalse: number;

  constructor(
    private tarefaService: TarefaService,
    private navCtrl: NavController,
    private overlayService: OverlayService,
    private emailComposer: EmailComposer
  ) {}

  async ngOnInit(): Promise<void> {
    const loading = await this.overlayService.loading();
    this.tarefas$ = this.tarefaService.getAll();
    this.tarefas$.pipe(take(1)).subscribe(tarefa => loading.dismiss());
    this.tarefas$.subscribe(
      result => {
        this.qtTarefasFalse = 0;
        for (const key in result) {
          if (result.hasOwnProperty(key)) {
            const element = result[key];
            if (!element.done) {
              this.qtTarefasFalse += 1;
            }
          }
        }
      }
    );
  }

  onUpdate(tarefa: Tarefa): void {
    this.navCtrl.navigateForward(`/tarefas/editar/${tarefa.id}`);
  }

  async onDelete(tarefa: Tarefa): Promise<void> {
    await this.overlayService.alter({
      message: `Deseja realmente deletar a tarefa ${tarefa.title} ?`,
      buttons: [
        {
          text: 'Sim',
          handler: async () => {
            await this.tarefaService.delete(tarefa);
            await this.overlayService.toast({
              message: `Tarefa "${tarefa.title}" deletada`
            });
          }
        },
        'Não'
      ]
    });
  }

  async onDone(tarefa: Tarefa): Promise<void> {
    const tarefaToUpdate = { ...tarefa, done: !tarefa.done };
    await this.tarefaService.update(tarefaToUpdate);
    await this.overlayService.alter({
      message: `Tarefa "${tarefa.title}" ${
        tarefaToUpdate.done
          ? 'Completada, deseja enviar email?'
          : 'Atualizada, deseja enviar email?'
      }`,
      buttons: [
        {
          text: 'Sim',
          handler: async () => {
            await this.email(tarefa);
            await this.overlayService.toast({
              message: `Tarefa "${tarefa.title}" enviada por email.`
            });
          }
        },
        'Não'
      ]
    });
  }
  email(dados: Tarefa) {
    this.emailComposer.addAlias('gmail', 'com.google.android.gm');
    const email = {
      app: 'gmail',
      to: 'luluka.magaiver@gmail.com',
      cc: '',
      bcc: [],
      attachments: [],
      subject: `Tarefa "${dados.title}"`,
      body: `Notificação de Tarefa ${!dados.done ? 'Completada' : 'Atualizada'}`,
      isHtml: true
    };
    this.emailComposer.open(email);
  }
}
