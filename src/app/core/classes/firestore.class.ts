import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection, QueryFn } from '@angular/fire/firestore';

export abstract class Firestore<T extends { id: string }> {
  protected colection: AngularFirestoreCollection<T>;

  constructor(protected db: AngularFirestore) {}

  protected setConlections(path: string, queryFn?: QueryFn): void {
    this.colection = path ? this.db.collection(path, queryFn) : null;
  }

  private setItem(item: T, operations: string): Promise<T> {
    this.colection
      .doc<T>(item.id)
      [operations](item)
      .then(() => item);
    return Promise.resolve(null);
  }

  getAll(): Observable<T[]> {
    return this.colection.valueChanges();
  }

  get(id: string): Observable<T> {
    return this.colection.doc<T>(id).valueChanges();
  }

  create(item: T): Promise<T> {
    item.id = this.db.createId();
    return this.setItem(item, 'set');
  }

  update(item: T): Promise<T> {
    return this.setItem(item, 'update');
  }

  delete(item: T): Promise<void> {
    return this.colection.doc<T>(item.id).delete();
  }
}
