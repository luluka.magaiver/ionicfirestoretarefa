import { AuthService } from './../../../core/service/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { NavController, MenuController } from '@ionic/angular';
import { OverlayService } from 'src/app/core/service/overlay.service';

@Component({
  selector: 'app-logout-button',
  template: `
    <ion-buttons>
      <ion-button (click)="logout()">
        <ion-icon name="exit" slot="icon-only"></ion-icon>
      </ion-button>
    </ion-buttons>
  `
})
export class LogoutButtonComponent implements OnInit {
  @Input() menuId: string;
  constructor(
    private authService: AuthService,
    private naveCtrl: NavController,
    private menuCtrl: MenuController,
    private overlayService: OverlayService
  ) {}

  async ngOnInit(): Promise<void> {
    if (!(await this.menuCtrl.isEnabled(this.menuId))) {
      this.menuCtrl.enable(true, this.menuId);
    }
  }

  async logout() {
    await this.overlayService.alter({
      message: 'Deseja sair da Aplicação ?',
      buttons: [
        {
          text: 'Sim',
          handler: async () => {
            await this.authService.logout();
            await this.menuCtrl.enable(false, this.menuId);
            this.naveCtrl.navigateRoot('/login');
          }
        },
        'Não'
      ]
    });
  }
}
