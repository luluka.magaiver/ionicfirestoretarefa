App de Tarefas com Firebase
Curso de Ionic 4 da Udemy
Professor Plinio

## rodar integracao

- ionic integrations enable cordova --add

## plataforma add

- ionic cordova platform add android

## plataforma remover

- ionic cordova platform rm android

## Rodar no celular

- ionic cordova run android --device

## Firebase CLI

- npm install -D firebase-tools@ˆ6

## Add Ionic 4 Cordova Native FCM Plugin
- ionic cordova plugin add cordova-plugin-fcm-with-dependecy-updated
- npm install @ionic-native/fcm
